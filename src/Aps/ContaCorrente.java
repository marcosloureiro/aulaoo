package Aps;

public class ContaCorrente extends ContaBancaria implements Imprimivel {
	private double taxaDeOperacao = 0.05;

	public ContaCorrente(long numeroConta, double saldo) {
		super(numeroConta, saldo);
	}

	public void sacar(double valor) {
		setSaldo(getSaldo() - valor - (valor*taxaDeOperacao));
	}	
	
	public double getTaxaDeOperacao() {
		return taxaDeOperacao;
	}

	public void setTaxaDeOperacao(double taxaDeOperacao) {
		this.taxaDeOperacao = taxaDeOperacao;
	}

	public void mostrarDados() {
		
	}
}
