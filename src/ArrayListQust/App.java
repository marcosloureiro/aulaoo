package ArrayListQust;

import java.util.*;

public class App {
	
	static List<Pessoa> lista = new ArrayList<Pessoa>();
	
	public static void main(String[] args) {
		lista.add(new Pessoa(1, "Jo�o", 10));
		lista.add(new Pessoa(2, "Alice", 5));
		lista.add(new Pessoa(4, "Carlos", 12));
		lista.add(new Pessoa(3, "Fernando", 27));
		lista.add(new Pessoa(5, "Priscila", 31));
		
		Collections.sort(lista, new Pessoa());
		
	}
}
