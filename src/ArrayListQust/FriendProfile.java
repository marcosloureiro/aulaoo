package ArrayListQust;
import java.util.Comparator;

public class FriendProfile implements Comparator<Pessoa> {    
    @Override
    public int compare(Pessoa o1, Pessoa o2) {
            if(o1.getIdade() > o2.getIdade()){
                return 1;
            }
            return 0;
    }
}