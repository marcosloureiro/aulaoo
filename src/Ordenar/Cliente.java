package Ordenar;

public class Cliente {
	private int numero;
	private String nome;
	private int votos;
	
	public Cliente(String nome, int numero) {
		this.numero = numero;
		this.nome = nome;
		this.votos = 0;
	}
	
	public void votar() {
		votos++;
	}
	
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getVotos() {
		return votos;
	}
	public void setVotos(int votos) {
		this.votos = votos;
	}

}
