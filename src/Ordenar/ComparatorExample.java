package Ordenar;

import java.util.Arrays;
import java.util.Comparator;

public class ComparatorExample {

	public static void main(String[] args) {
		
		Cliente[] clientes = {
				new Cliente("Bruno", 30),
				new Cliente("Maria", 28),
				new Cliente("Carlos", 40)
		};
		clientes[0].votar();
		clientes[0].votar();
		clientes[1].votar();
		clientes[0].votar();
		clientes[0].votar();
		clientes[2].votar();
		clientes[0].votar();
		clientes[0].votar();
		clientes[1].votar();
		clientes[1].votar();
		clientes[0].votar();
		clientes[2].votar();
		Arrays.sort(clientes, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente b1, Cliente b2) {
				return b1.getNome().compareTo(b2.getNome());
			}
		});
		imprime(clientes);

		Arrays.sort(clientes, new Comparator<Cliente>() {
			@Override
			public int compare(Cliente b1, Cliente b2) {
				if (b1.getVotos() > b1.getVotos()) return 1;
				if (b1.getVotos() < b2.getVotos()) return -1;
				return 0;
			}
		});
		imprime(clientes);
			
	}
	
	private static void imprime(Cliente[] clientes) {
		for (Cliente cliente : clientes) {
			System.out.print(cliente.getNome());
			System.out.print("(" + cliente.getVotos() + "),");
		}
		System.out.println();
	}
	
}
