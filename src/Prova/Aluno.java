package Prova;

public class Aluno {
	private String nome;
	private int matricula;
	private float AV1, AV2;
	
	public Aluno(String nome, int matricula) {
		setNome(nome);
		setMatricula(matricula);
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getMatricula() {
		return matricula;
	}
	public void setMatricula(int matricula) {
		this.matricula = matricula;
	}
	public float getAV1() {
		return AV1;
	}
	public void setAV1(float aV1) {
		AV1 = aV1;
	}
	public float getAV2() {
		return AV2;
	}
	public void setAV2(float aV2) {
		AV2 = aV2;
	}
	public float calculaMedia(float av1, float av2) {
		float media = (av1+av2)/2;
		return media;
	}
	public String aprovacao(float media) {
		if(media < 7) {
			return "REPROVADO";
		}else{
			return "APROVADO";
		}
	}
	
}
