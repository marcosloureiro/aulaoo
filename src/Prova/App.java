package Prova;

import java.util.Scanner;

import POO_VECTOR.Conta;

public class App {
    static final int MAXALUNO = 20;
    static int index = 0;
    
    static Aluno[] lista = new Aluno[MAXALUNO];
    
    static Scanner tecla = new Scanner(System.in);

	public static void main(String[] args) {
        int op;
        do {                
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir aluno");
            System.out.println("2-Excluir");
            System.out.println("3-Lancar nota");
            System.out.println("4-Listar");
            System.out.println("5-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
            switch(op){
                case 1: inserirAluno(); break;
                case 2: excluirAluno(); break;
                case 3: lancarNota(); break;    
                case 4: listarAlunos(); break;
                case 5: break;
            }
        } while (op!=5);       
		
	}
	
	public static void inserirAluno() {
		System.out.println("Insira o nome do Aluno:");
        tecla.nextLine();
        String nome = tecla.nextLine();
        System.out.println("Digite o n�mero da matricula:");
        int matricula = tecla.nextInt();
        lista[index++] = new Aluno(nome, matricula);
        System.out.println("Aluno cadastrado com sucesso!");		
	}
	
    public static void excluirAluno(){
        System.out.println("Digite a matricula:");
        int num = tecla.nextInt();
        
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getMatricula()){
                lista[i] = null;
                break;
            }
        }
    }
    
    public static void lancarNota(){
        System.out.println("Digite a matricula:");
        int num = tecla.nextInt();
        
        for (int i = 0; i < lista.length-1; i++) {
            if (num == lista[i].getMatricula()){
                System.out.println("Digite a av1:");
                float av1 = tecla.nextFloat();
                System.out.println("Digite a av2:");
                float av2 = tecla.nextFloat();
                lista[i].setAV1(av1);
                lista[i].setAV2(av2);
                break;
            }
        }
    }
    
    public static void listarAlunos(){
	    for (int i = 0; i < lista.length-1; i++) {
	        if (lista[i] != null){
	        	float media = lista[i].calculaMedia(lista[i].getAV1(), lista[i].getAV2());
	        	System.out.println("Aluno: "+lista[i].getNome()+" - Media: "+ media + " | " + lista[i].aprovacao(media));
	        }else{
	            break;
	        }
	    }
    }
    
}
