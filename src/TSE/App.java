package TSE;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

import Ordenar.Cliente;

public class App {
	
	static final int LIMITE  = 10;
	static candidatoTSE[] candidatos = new candidatoTSE[LIMITE];
	static Scanner teclado = new Scanner(System.in);
	static int atualCand = 0;
	static int totalVotos = 0;
	static float[] porcentagem = new float[LIMITE];

	public static void main(String[] args) {
		// INSERIR DADOS
		candidatos[atualCand++] = new candidatoTSE(30, "Marcos");
		candidatos[atualCand++] = new candidatoTSE(20, "Doria");
		totalVotos++;
		candidatos[0].votar();
		totalVotos++;
		candidatos[1].votar();
		totalVotos++;
		candidatos[1].votar();
		totalVotos++;
		candidatos[1].votar();
		
		int op;
		do {
	        System.out.println("");
	        System.out.println("*** MENU PRINCIPAL ***");
	        System.out.println("1-Incluir candidato");
	        System.out.println("2-Listar candidatos");
	        System.out.println("3-Mostrar resultado");
	        System.out.println("4-Votar");
	        System.out.println("5-Sair");
	        System.out.println("Digite sua op��o: ");
	        op = teclado.nextInt(); 
	        switch(op){
	            case 1: criarCandidato(); break;
	            case 2: listarCandidados(); break;
	            case 3: mostrarResultado(); break;    
	            case 4: efetuarVoto(); break;
	            case 5: sair(); break;
	        }
		
		} while (op < 5);
		
	}
	
	public static void criarCandidato() {
		if(atualCand < 10) {
			System.out.println("");
			System.out.println("CADASTRAR CANDIDATO");
			System.out.println("**********************");
			System.out.println("Qual o nome do candidato?");
			teclado.nextLine();
			String nome = teclado.nextLine();
			System.out.println("Qual o NUMERO do candidato?");
			int numero = teclado.nextInt();
			candidatos[atualCand++] = new candidatoTSE(numero, nome);
		}
	}
	public static void listarCandidados() {
		System.out.println("");
		System.out.println("CANDIDADOS CADASTRADOS");
		System.out.println("**********************");
		for(int i = 0; i < candidatos.length-1; i++){
            if (candidatos[i] != null){
            	System.out.println("Nome: "+candidatos[i].getNome()+" | Numero: "+candidatos[i].getNumero()+" | Votos: "+ candidatos[i].getVotos());
            }
		}
	}
	public static void mostrarResultado() {
		Arrays.sort(candidatos, new Comparator<candidatoTSE>() {
		    @Override
		    public int compare(candidatoTSE b1, candidatoTSE b2) {
		        if (b1.getVotos() > b1.getVotos()) return 1;
		        if (b1.getVotos() < b2.getVotos()) return -1;
		        return 0;
		    }
		});
		System.out.println("");
		System.out.println("RESULTADO");
		System.out.println("**********************");
		
		if(totalVotos > 0) {
			for(int i = 0; i < candidatos.length-1; i++){
	            if (candidatos[i] != null){
	            	float perc = ((float)candidatos[i].getVotos()/(float)totalVotos)*100;
	            	porcentagem[i] = perc;
	            	DecimalFormat df = new DecimalFormat("#.00");
	            	String percStr = df.format(perc);
	            	System.out.println("Nome: "+candidatos[i].getNome()+" | Numero: "+candidatos[i].getNumero()+" | Votos: "+candidatos[i].getVotos()+" | "+percStr+"%");
	            }
			}
			for(int i = 0; i < porcentagem.length-1; i++) {
				
			}
			System.out.println("---------------------");
			System.out.println("TOTAL DE VOTOS: "+totalVotos);
		}else{
			System.out.println("SEM VOTOS NO SISTEMA!");
		}
	}
	public static void efetuarVoto() {
		totalVotos++;
		boolean encontrado = false;
		System.out.println("");
		System.out.println("VOTAR");
		System.out.println("**********************");
		System.out.println("Qual o NUMERO do candidato?");
		int numero = teclado.nextInt();
		for(int i = 0; i < candidatos.length-1; i++){
            if (candidatos[i] != null){
                if (candidatos[i].getNumero() == numero){
	            	encontrado = true;
	            	candidatos[i].votar();
	    			System.out.println("Voto computado para candidato "+candidatos[i].getNome());
	    			break;
	            }
            }
		}
		if(!encontrado) {
			System.out.println("Candidato n�o encontrado");
		}
	}
	public static void sair() {
		System.out.println("OBRIGADO POR PARTICIPAR");
	}
}
