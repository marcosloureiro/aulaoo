package TesteMarcos;
import java.util.Scanner;

public class App {
	public static Scanner tecla = new Scanner(System.in);
	public static int quantMaxPessoa = 20;
	public static int pessoaAtual = 0;
	public static Pessoa[] pessoaId = new Pessoa[quantMaxPessoa];
	
	public static void main(String[] args) {
        int op;
		do {
            System.out.println("");
            System.out.println("*** MENU PRINCIPAL ***");
            System.out.println("1-Incluir pessoa");
            System.out.println("2-Vender ingresso");
            System.out.println("3-Alterar quantidade");
            System.out.println("4-Fechar sistema");
            System.out.println("5-Sair");
            System.out.println("Digite sua op��o: ");
            op = tecla.nextInt(); 
	        switch(op){
	            case 1: incluirPessoa(); break;
	            case 2: venderIngresso(); break;
	            case 3: alterarQuantidade(); break;    
	            case 4: fecharSistema(); break;
	            case 5: break;
	        }
			
		} while (op < 4);
	}
	
	public static void incluirPessoa() {
        System.out.println("");
        System.out.println("Digite o nome:");
        tecla.nextLine();
        String nome = tecla.nextLine();
        System.out.println("Digite a idade");
        int idade = tecla.nextInt();
        pessoaAtual++;
        Pessoa pessoa = new Pessoa(nome, idade, pessoaAtual);
        
        pessoaId[pessoaAtual] = pessoa;
        System.out.println("A pessoa "+pessoaId[pessoaAtual].getNome()+" foi inserida com sucesso sob o ID "+pessoaId[pessoaAtual].getId()+"!");
        System.out.println("");
	}
	
	public static void venderIngresso() {
        System.out.println("Digite o ID da pessoa: ");
        int idPessoa = tecla.nextInt();
		
        Ingresso ingresso = new Ingresso(pessoaId[idPessoa]);
        System.out.println("O ingresso foi adicionado para "+ingresso.getPessoa().getNome()+" com o c�digo "+ingresso.ingresso[ingresso.getId()]+"!");
        System.out.println("");
	}
	
	public static void alterarQuantidade() {
		
	}
	
	public static void fecharSistema() {
		
	}

}
