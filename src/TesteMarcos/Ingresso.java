package TesteMarcos;

public class Ingresso {
	public static boolean aberto = true;
	public static int quantAtual = 0;
	public static int quantidadeMax = 10;
	public static String[] ingresso = new String[quantidadeMax];
	private int id;
	private Pessoa pessoa;
	
	public Ingresso(Pessoa pessoa) {
		if(aberto) {
			if(quantAtual < quantidadeMax) {
				quantAtual++;
				ingresso[quantAtual] = gerarCodigo();
				this.id = quantAtual;
				this.pessoa = pessoa;
			}
		}
	}
	
	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	private String gerarCodigo() { 

	    String[] carct ={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
	    
	    String senha="";
	    
	    for (int x=0; x<10; x++){
	        int j = (int) (Math.random()*carct.length);
	        senha += carct[j];
	    }
	    return senha;
    }
}
