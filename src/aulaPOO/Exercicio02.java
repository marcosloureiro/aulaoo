package aulaPOO;
import java.util.Scanner;

public class Exercicio02 {
	public static void main(String[] args) {
		float temp_f = 0, temp_c = 0;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Informe a temperatura em graus Fahrenheit: ");
		temp_f = teclado.nextFloat();
		temp_c = ((temp_f - 32) * 5) / 9;
		System.out.println("A temperatua em graus Celsius e: " + temp_c);		
	}
}
