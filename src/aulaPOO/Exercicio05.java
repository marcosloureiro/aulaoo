package aulaPOO;

import java.util.Scanner;

public class Exercicio05 {

	public static void main(String[] args) {
		double comp, larg, alt, area;
		int caixas;
		
		comp = 0;
		larg = 0;
		alt = 0;
		area = 0;
		caixas = 0;
		
		Scanner teclado = new Scanner(System.in);
		
		System.out.println("Qual o comprimento da cozinha? ");
		comp = teclado.nextDouble();
		System.out.println("Qual a largura da cozinha? ");
		larg = teclado.nextDouble();
		System.out.println("Qual a altura da cozinha? ");
		alt = teclado.nextDouble();
		
		area = (comp*alt*2) + (larg*alt*2);
		caixas = (int)Math.round(area/1.5);
		
		System.out.println("Quantidade de caixas de azulejos para colocar em todas as paredes: "+caixas);
		
	}

}
