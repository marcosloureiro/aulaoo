package aulaPOO;

import java.util.Scanner;

public class Exercicio06 {

	public static void main(String[] args) {
		double odom_i, odom_f, litros, valor_t, media, lucro, gasol_l;
		
		odom_i = 0;
		odom_f = 0;
		litros = 0;
		valor_t = 0;
		media = 0;
		lucro = 0;
		gasol_l = 1.90;
		
		Scanner telcado = new Scanner(System.in);
		
		System.out.println("Marcacao inicial do odometro (Km): ");
		odom_i = telcado.nextDouble();
		System.out.println("('Marcacao final do odometro (Km): ");
		odom_f = telcado.nextDouble();
		System.out.println("Quantidade de combustivel gasto (litros): ");
		litros = telcado.nextDouble();
		System.out.println("Valor total recebido (R$): ");
		valor_t = telcado.nextDouble();
		
		media = (odom_f - odom_i)/litros;
		lucro = valor_t - (litros * gasol_l);
		
		System.out.println("");
		System.out.println("Media de consumo em Km/L: "+Math.round(media));
		System.out.println("Lucro (liquido) do dia: R$"+Math.pow(lucro,2));

	}

}
