package aulaPOO;
import java.util.Scanner;
public class ExercicioRep01 {
	public static void main(String[] args) {
		int a, b;
		float divisao;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Digite um valor inteiro:");
		a = teclado.nextInt();
		do {
			System.out.println("Digite outro valor inteiro:");
			b = teclado.nextInt();
		} while (b == 0);
		
		divisao = a/b;
		
		System.out.println("A divisao do primeiro valor pelo segundo �:"+divisao);
	}
}
