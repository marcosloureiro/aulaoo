package aulaPOO;
import java.util.Scanner;

public class ExercicioSel05 {
	public static void main(String[] args) {
		float valor;
		Scanner teclado = new Scanner(System.in);
		System.out.println("Digite um valor:");
		valor = teclado.nextFloat();
		if(valor < 0) {
			System.out.println("NEGATIVO");
		}else{
			System.out.println("POSITIVO");
		}
	}
}